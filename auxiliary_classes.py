"""Auxiliary classes for the card building game.

This module contains auxiliary classes that are used by the card building 
game.

Classes:
    Card: Represents a card of the game.
    Player: Represents a player of the game.
    CentralLines: Represents the central line of cards.

Docstring conventions followed in this module can be found online at:
https://www.python.org/dev/peps/pep-0257/
"""
import itertools
import random

class Card(object):
    """Represents a card of the game.

    This class is responsible for the creation and initialisation of a card
    object. It implements getters that return a card's indivudal stats and 
    another method that returns the attributes of the card concatenated as 
    a string.
    """

    def __init__(self, name, values=(0, 0), cost=1):
        """Initialises a card object.

        Keyword arguments:
            name -- the name of the card
            values -- the attack and money values of the card (default 0,0).
            cost -- the cost of the card (default 1).
        """

        self.name = name
        self.cost = cost
        self.values = values

    def __str__(self):
        """Return the attributes of a card concatenated as a string."""
        return ('%s costing %s with attack %s and money %s' % 
		(self.name, self.cost, self.values[0], self.values[1]))

    def get_name(self):
        """return the name of the card """
        return self.name

    def get_attack(self):
        """return the attack value of the card """
        return self.values[0]

    def get_money(self):
        """return the money value of the card"""
        return self.values[1]

class Player(object):
    """Represents a player of the game.
    
    This class is responsible for the creation and initialisation of a player
    (computer or human) of the card building game. It implements getters and
    setters for each individual attribute and additional utility methods.
    """ 

    def __init__(self, name, health, deck, hand, active, handSize, discard, money, attack):
        """Initialises a Player object.

        The instance variables of the class are initialised according to the
        values passed as arguments. If the deck argument is passed as None,
        a list of 10 cards is created, shuffled and assigned to this class's
        deck instance variable.

        Keyword arguments:
            name -- the name of the player.
            health -- the health of the player.
            deck -- the set of cards that are currently at the deck.
            hand -- the set of cards that are currently in hand.
            active -- the set of cards that are currently at the active area.
            handSize -- the total number of cards that can be in the hand.
            discard -- the set of cards that are currently at the discard 
            pile.
            money -- the player's total money
            attack -- the player's total attack power
        """
            
        self.name = name
        self.health = health
        if deck is None:
            initialDeck = [
                           8 * [Card('Serf', (0, 1), 0)],
                           2 * [Card('Squire', (1, 0), 0)]
                          ]
            self.deck = list(itertools.chain.from_iterable(initialDeck))
            random.shuffle(self.deck)
        else:
            self.deck=deck
        self.hand=hand
        self.active = active
        self.handSize = handSize
        self.discard = discard
        self.money = money
        self.attack = attack
	
    def get_name(self):
        """Return the name of the player."""
        return self.name
	
    def get_health(self):
        """Return the health of the player."""
        return self.health

    def get_deck(self):
        """Return the cards that are currently at the deck."""
        return self.deck
	
    def get_hand(self):
        """Return the cards that are currently in hand."""
        return self.hand

    def get_active(self):
        """Return the cards that are currently at the active area."""
        return self.active

    def get_handSize(self):
        """Return the total number of cards that can be in the hand."""
        return self.handSize

    def get_discard(self):
        """Return the cards that are currently at the discard pile."""
        return self.discard

    def get_money(self):
        """Return the value of the money of the player"""
        return self.money

    def get_attack(self):
        """Return the value of the attack power of the player"""
        return self.attack

    def set_name(self, name):
        """Set the name of the player."""
        self.name=name
	
    def set_health(self, health):
        """Set the health of the player."""
        self.health=health

    def set_deck(self, deck):
        """Set the set of cards at the deck."""
        self.deck=deck
	
    def set_hand(self, hand):
        """Set the set of cards in hand."""
        self.hand=hand

    def set_active(self, active):
        """Set the set of cards  at the active area."""
        self.active=active

    def set_handSize(self, handSize):
        """Set the number of the cards that can be in hand."""
        self.handSize=handSize

    def set_discard(self, discard):
        """Set the set of cards at the discard pile."""
        self.discard=discard
  
    def set_money(self, money):
        """Set the value of money of the player"""
        self.money=money

    def set_attack(self, attack):
        """Set the value of the attack power of the player"""
        self.attack=attack    

    def draw_cards(self):
        """Draw cards from deck and place them in player's hand.
 
        This method draws cards from the player's deck and places them
        in their hand. If the deck becomes empty, the discard pile gets
        shuffled and the discard pile's cards get transferred to the deck.
        """

        for x in range(0, self.get_handSize()):
            if (len(self.get_deck()) == 0):
                random.shuffle(self.get_discard())
                self.set_deck(self.get_discard())
                self.set_discard([])
            card = self.get_deck().pop()
            self.get_hand().append(card)
   
    def print_hand(self):
        """Print player's cards that are currently in their hand"""
        index = 0
        for card in self.get_hand():
            print "[%s] %s" % (index, card)
            index = index + 1

    def print_active(self):
         """Print the player's cards that are currently at the active area"""
         for card in self.get_active():
             print card

    def play_cards(self):
        """Move every card from hand to the active area. 

       This method iterates through all the cards that reside in player's hand
       and places them at the active area. Additionally, it updates the
       total values of money and attack of the player
       """

        if(len(self.get_hand())>0):
            for x in range(0, len(self.get_hand())):
                card = self.get_hand().pop()
                self.get_active().append(card)
                self.set_money(self.get_money() + card.get_money())
                self.set_attack(self.get_attack() + card.get_attack())

    def play_card(self, cardNumber):
        """Move a card from hand to the active area. 

        This method checks if the 'cardNumber' is smaller or equal to the cards
        in hand. If so, it moves the card indexed by 'cardNumber' from the 
        player's hand to their active cards and adds its attack and money 
        values to the player's total ones.

        Return:
             card.get_name() -- if the 'cardNumber' is valid return the name 
                                of the card
             None -- If cardNumber is invalid return None  
        """

        if( int(cardNumber) < len(self.get_hand())):
            card = self.get_hand().pop(int(cardNumber))
            self.get_active().append(card)
            self.set_money(self.get_money() + card.get_money())
            self.set_attack(self.get_attack() + card.get_attack())
            return card.get_name()
        else:
            return None

    def to_discard(self):
        """Transfer cards from hand and active area to the discard pile"""
        if (len(self.get_hand()) >0 ):
            for x in range(0, len(self.get_hand())):
                self.get_discard().append(self.get_hand().pop())

        if (len(self.get_active()) >0 ):
            for x in range(0, len(self.get_active())):
                self.get_discard().append(self.get_active().pop())

    

class CentralLine(object):
    """Represents the central line of the game.
    
    This class is responsible for the creation and initialisation of the 
    central line of the game. It implements getters and setters for each
    individual attribute and additional utility methods.
    """ 

    def __init__(self, name, active, activeSize, supplement, deck):
        """Initialises the CentralLine object.

        The instance variables of the class are initialised according to the
        values passed as arguments. If the deck argument is passed as None,
        a list of 36 cards is created, shuffled and assigned to this class's
        deck instance variable. If the supplement argument is passed as None,
        a list of 10 identical cards is created and assigned to this class's
        supplement instance variable.

        Keyword arguments:
            name -- the name of the central line.
            active -- the set of cards that are currently active at the 
                      central line.
            activeSize -- the number of cards that can active in the central
                          line.
            handSize -- the set of supplement cards.
            deck -- the set of cards that are currently at the deck of the
                    central line.
        """
        self.name = name
        self.active = active
        self.activeSize = activeSize
        if supplement is None:
            self.supplement = 10 * [Card('Levy', (1, 2), 2)]
        else:
            self.supplement=supplement
        if deck is None:
            initialDeck = [
                           4 * [Card('Archer', (3, 0), 2)], 
                           4 * [Card('Baker', (0, 3), 2)], 
                           3 * [Card('Swordsman', (4, 0), 3)], 
                           2 * [Card('Knight', (6, 0), 5)],
                           3 * [Card('Tailor', (0, 4), 3)],
                           3 * [Card('Crossbowman', (4, 0), 3)],
                           3 * [Card('Merchant', (0, 5), 4)],
                           4 * [Card('Thug', (2, 0), 1)],
                           4 * [Card('Thief', (1, 1), 1)],
                           2 * [Card('Catapault', (7, 0), 6)], 
                           2 * [Card('Caravan', (1, 5), 5)],
                           2 * [Card('Assassin', (5, 0), 4)]
                          ]
            self.deck = list(itertools.chain.from_iterable(initialDeck))
            random.shuffle(self.deck)
        else:
            self.deck = deck
	
    def get_name(self):
        """Return the name of the central line."""
        return self.name	

    def get_active(self):
        """Return the set of cards that are currently at the central line."""
        return self.active

    def get_activeSize(self):
        """Return number of cards that can be active at the central line"""
        return self.activeSize
	
    def get_supplement(self):
        """Return the set of supplement cards."""
        return self.supplement

    def get_deck(self):
        """Return the set of cards that are currently at the deck."""
        return self.deck


    def set_name(self, name):
        """Set the name of the central line."""
        self.name=name
	
    def set_active(self, active):
        """Set the set of cards at the central line."""
        self.active=active

    def set_activeSize(self, activeSize):
        """Return the number of the cards that can be active at the central line."""
        self.handSize=activeSize

    def set_supplement(self, supplement):
        """Return the set of cards at the supplement."""
        self.supplement=supplement

    def set_deck(self, deck):
        """Set the set of cards at the deck."""
        self.deck=deck
    
    def draw_active(self):
        """Draw cards from (main) deck and place them in active area."""  
        for x in range(0, self.get_activeSize()):
            card = self.get_deck().pop()
            self.get_active().append(card)

    def print_active(self):
        """Print central line's active cards."""
        index=0
        for card in self.get_active():
            print "[%s] %s" % (index, card)
            index = index + 1

    def print_supplement(self):
        """Print a central line's supplement card."""
        if len(self.get_supplement()) > 0:
            print self.get_supplement()[0]
















