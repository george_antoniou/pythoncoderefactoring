"""Auxiliary functions for the card building game.

This module contains auxiliary functions that are used by the card building 
game.

Functions:
    buy_supplement: Transfers card from central line's supplement to player
    buy_active: Transfers card from central line's active to player 
    print_main_menu: Prints the main menu of the game.
    print_buy_meny: Prints the player's buy menu.
    print_computer_menu: Prints the computer's menu.
    winner: Checks whether the game is over and prints the winner.
    start_game: Asks player if they want to start a new game.
    opponent_type: Asks the player to choose opponent type.
    computer_purchase: Implements computer card purchase.

Docstring conventions followed in this module can be found online at:
https://www.python.org/dev/peps/pep-0257/
"""

import os

def buy_supplement(player, centralLine):
    """Buy card from the central line's supplement
    
    This function checks whether there are available cards at the central
    line's supplement, and whether the player has enough money to buy it.
    If so, the card is transfered from the supplement to the player's 
    discard pile, and the player's money are reduced by the card's cost.
    
    Keyword arguments:
       player -- the player who wants to buy the supplement card
       centralLine -- the central line of the game 
    """
    if len(centralLine.get_supplement()) > 0:
        if player.get_money() >= centralLine.get_supplement()[0].cost:
            player.set_money(player.get_money() - 
                             centralLine.get_supplement()[0].cost)
            card = centralLine.get_supplement().pop()
            player.get_discard().append(card)
            os.system('clear')
            print "You have bought the card %s!" % (card.get_name())
        else:
            os.system('clear')
            print "Insufficient money to buy"
    else:
        os.system('clear')
        print "No supplements left"

def buy_active(player, centralLine, cardNumber):
    """Buy card from the central line's active cards
    
    This function checks whether whether the player
    has enough money to buy the card. If so, the card is transfered from
    the central line to the player's discard pile and the player's money are 
    reduced by the card's cost.

    Keyword arguments:
        player -- the player who wants to buy the card
        centralLine -- the central line of the game
        cardNumber -- the index number of the card 
    """
    if player.get_money() >= centralLine.get_active()[int(cardNumber)].cost:
        player.set_money(player.get_money() - 
        centralLine.get_active()[int(cardNumber)].cost)
        boughtCard = centralLine.get_active().pop(int(cardNumber))
        player.get_discard().append(boughtCard)
        if len(centralLine.get_deck()) > 0:
            card = centralLine.get_deck().pop()
            centralLine.get_active().append(card)
        else:
            centralLine.set_activeSize(centralLine.get_activeSize()-1)
        os.system('clear')
        print "You have bought the card %s!" % (boughtCard.get_name())
    else:
        os.system('clear')
        print ("Insufficient money to buy, press B if you want to choose " 
        "another card")
        
def print_main_menu(player, computer):
    """Print the game menu.

    This function prints both players's health, the human player's money value,
    attack value, cards in active area, cards in hand, and the human player's 
    available choices

    Keyword arguments:
       player -- the human player
       computer -- the computer opponent
    """
    print "\nPlayer Health: %s" % player.get_health()
    print "Computer Health: %s" % computer.get_health()
    print "\nYour money %s:" % (player.get_money())
    print "Your attack power: %s" % (player.get_attack())
    print "\nYour Active Cards:"
    player.print_active()
    print "\nYour Hand:"
    player.print_hand()
    print ("\nChoose Action: (P = Play all, [0-%s] = Play that card, "   
           "B = Buy Card, A = Attack, E = End turn, Q = Quit game)" % 
            (len(player.get_hand())))

def print_buy_menu(player, centralLine):
    """Print player's buy menu
    
    Keyword arguments:
       player -- the player who wants to buy the card
       centralLine -- the central line of the game
    """

    os.system('clear')
    print "Your money: %s" % (player.get_money())
    print "\nAvailable Cards:"
    centralLine.print_active()
    print "\nSupplement:"
    centralLine.print_supplement()
    print ("\nChoose a card to buy [0-%s], S for supplement, E to "
            "end buying" % (len(centralLine.get_active())))


def print_computer_menu(computer):
    """Print the computer menu.

    This functions prints the active cards, the money and attack value, 
    the results of the attack and the purchases of the computer.
    
    Keyword arguments:
        computer -- the opponent of the player
    """
    print "Computer's active cards: "
    computer.print_active()
    print "\nComputer money %s:" % (computer.get_money())
    print "Computer attack power: %s" % (computer.get_attack())
    print "\nComputer's actions:"
    print "The computer attacked you and dealt %s damage!" % (computer.get_attack())

def winner(player, computer, centralLine):
    """Return the winner if the game is over

    This function checks whether the game is over by checking if the health
    of a player is 0 or lower or if there are no more cards at the central
    line. If a player's health reaches zero, the other one wins. If the 
    central line cards are over, the player with the highest health at the 
    moment wins. If both players have the same health, the player with the largest 
    total value of attack strength wins. If both players have the same strength, 
    there is a draw.
    
    There is a possibility that both players reach negative health in the same 
    round. The same rules apply in this case as well: the winner is decided 
    based on highest health or card strength. 

    Keyword arguments:
        player -- the human player
        computer -- the opponent of the player
        centralLine -- the central line of cards

    Return:
        continueGame -- True if both players health is above zero and if
                        there are still cards at the central line. False
                        otherwise
    """

    continueGame = True
    if player.get_health() <= 0 and computer.get_health() <= 0:
        if player.get_health() < computer.get_health():
            print "Computer wins"
        elif player.get_health() > computer.get_health():
            print "Congratulations!! You have won!"
        else:
            playerStrength = 0
            computerStrength = 0
            for card in player.get_discard():
                playerStrength = playerStrength + card.get_attack()
            for card in computer.get_discard():
                computerStrength = computerStrength + card.get_attack()
            if playerStrength > computerStrength:
                print "Congratulations!! You have won on card strength!"
            elif computerStrength > playerStrength:
                print "Computer wins on card strength"
            else:
                print "Draw"
        continueGame = False
    elif player.get_health() <= 0:
        continueGame = False
        print "Computer wins"
    elif computer.get_health() <= 0:
        continueGame = False
        print 'Congratulations!! You have won!'
    elif centralLine.get_activeSize() == 0:
        print "No more cards available"
        if player.get_health() > computer.get_health():
            print "Congratulations!! You have won on health!"
        elif computer.get_health() > player.get_health():
            print "Computer wins on health"
        else:
            playerStrength = 0
            computerStrength = 0
            for card in player.get_discard():
                playerStrength = playerStrength + card.get_attack()
            for card in computer.get_discard():
                computerStrength = computerStrength + card.get_attack()
            if playerStrength > computerStrength:
                print "Congratulations!! You have won on card strength!"
            elif computerStrength > playerStrength:
                print "Computer wins on card strength"
            else:
                print "Draw"
        continueGame = False
    return continueGame


def start_new_game():
    """Ask the user if they wish to start a new game
 
    This function asks the user whether they want to start a new game, and 
    checks that the entered value is correct (Y or N including lowercase). 

    Return:
         continueGame -- true if the user enters 'y', false otherwise
    """
    os.system('clear')
    playGame = raw_input('Do you want to play a game?(Y/N)')
    while playGame.lower() != 'y' and playGame.lower() != 'n':
        os.system('clear')
        playGame = raw_input('Please enter a valid option - Y for Yes, N for ' 
                             'No')
    os.system('clear')
    continueGame = (playGame.lower()=='y')
    return continueGame

def opponent_type():
    """Ask the user to pick the type of their opponent
 
       This function asks the user whether they want an aggressive or, 
       acquantitative opponent and checks that the entered value is
       correct (A or Q including lowercase). 

       Return:
           aggressive -- true if the user chooses aggressive, false otherwise
    """

    opponentType = raw_input("Do you want an aggressive (A) opponent or an" 
                   "acquisative (Q) opponent")
    while opponentType.lower() != 'a' and opponentType.lower() != 'q':
        os.system('clear')
        opponentType = raw_input('Please enter a valid option - A for ' 
                                  'aggressive, Q for acquisative')
    if opponentType.lower() == 'a':
        os.system('clear')
        print "You chose an aggresive opponent!"
        aggressive = True
    else:
        os.system('clear')
        print "You chose an acquisative opponent!"
        aggressive = True  
    return aggressive  

def computer_purchase(computer, centralLine, aggressive):
    """Purchase all possible cards for the computer

	This function implements an algorithm thar purchaces cards from the
    central line for the computer. The algorithm initially appends to a
    list all the cards from the central line's active set and supplement
    that can be bought by the computer (the cards that the computer has
    enough money for). After that it selects the more expensive cart from
    the list. In case of ties, if the computer was chose as 'aggressive', 
    the attack strength of the two cards is compared to break the tie. 
    Otherwise, the money are used. Finally, the algorithm transfers the 
    card from the central line's active set or supplement (depending 
    on where was the most expensive cart) to the computer's discard pile
    and the money of the computer are reduced by the bought card's cost.
    The algorithm continues until there is not any card left than can be 
    purchased.

    Keyword arguments:
       computer -- the players opponent
       centralLine -- the central line of cards
       aggressive -- true if the computer is aggressive, false otherwise
    """

    continueBuying = True
    boughtCard = False
    while continueBuying:
        cardList = []
        #check if supplement available at central line
        if len(centralLine.get_supplement()) > 0:
            #check if money suffice to buy supplement and mark accordingly
            if centralLine.get_supplement()[0].cost <= computer.get_money():
                cardList.append(("S", centralLine.get_supplement()[0]))
        #check which active cards can be bought and mark them
        for intindex in range (0, centralLine.get_activeSize()):
            if centralLine.get_active()[intindex].cost <=computer.get_money():
                cardList.append((intindex,centralLine.get_active()[intindex]))
        #check if the computer can buy at least 1 card
        if len(cardList) >0:
            boughtCard = True 
            highestIndex = 0
            #find most expensive card
            for intindex in range(0,len(cardList)):
                if cardList[intindex][1].cost >cardList[highestIndex][1].cost:
                    highestIndex = intindex
                #if same cost, decide based on attack or money
                if cardList[intindex][1].cost==cardList[highestIndex][1].cost:
                    if aggressive:
                        if (cardList[intindex][1].get_attack() >
                            cardList[highestIndex][1].get_attack()):
                            highestIndex = intindex
                    else:
                        if (cardList[intindex][1].get_money() > 
                            cardList[highestIndex][1].get_money()):
                            highestIndex = intindex

            source = cardList[highestIndex][0]
            #check if most expensive card is part of the active cards
            if source in range(0,5):
                #buy card from active and transfer it to the discard pile
                computer.set_money(computer.get_money() - 
                                   centralLine.get_active()[int(source)].cost)
                card = centralLine.get_active().pop(int(source))
                print "The computer bought the card %s!" % card
                computer.get_discard().append(card)
                #check if central line deck got empty
                if( len(centralLine.get_deck()) > 0):
                    card = centralLine.get_deck().pop()
                    centralLine.get_active().append(card)
                else:
                    centralLine.set_activeSize(centralLine.get_activeSize()-1)

            else:
                #buy card from supplement and transfer it to the discard pile
                computer.set_money(computer.get_money() - 
                                   centralLine.get_supplement()[0].cost)
                card = centralLine.get_supplement().pop()
                computer.get_discard().append(card)
                print "The computer bought the card %s!" % card
        else:
            continueBuying = False
            if boughtCard == False:
                print "The computer didn't have money to buy anything!"




        
















