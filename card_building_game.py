"""The main file for the card building game.

This module implements the logical sequence of the card building game.
More details about implementation are included in the imported auxiliary 
files.

Docstring conventions followed in this module can be found online at:
https://www.python.org/dev/peps/pep-0257/
"""

import os
from auxiliary_classes import Card, Player, CentralLine
from auxiliary_functions import buy_supplement, buy_active, print_main_menu
from auxiliary_functions import print_buy_menu, print_computer_menu, winner
from auxiliary_functions import start_new_game, opponent_type, computer_purchase

if __name__ == '__main__':
    #initialise objects and deal central line cards
    player=Player('human player', 30, None, [], [], 5, [],0,0)
    computer = Player('computer', 30, None, [], [], 5, [],0,0)
    centralLine= CentralLine('central line', [], 5, None, None)
    centralLine.draw_active()
	
    continueGame = start_new_game()
    if continueGame == True:
        aggressive = opponent_type()
    while continueGame:
        #initialise player stats and deal cards
        player.draw_cards()
        player.set_money(0)
        player.set_attack(0)
        while True:
            
            print_main_menu(player, computer)
            action = raw_input("Enter Action: ")
            if action.lower() == 'p':
                #transfer all cards from hand to the active area
                if len(player.get_hand())>0:
                    player.play_cards()
                    os.system('clear')
                    print "You played all of your cards!"
                else:
                    os.system('clear')
                    print "All cards have been played already!"
            elif action.isdigit():
                #transfer specific card from hand to the active area
                cardName = player.play_card(action)
                if cardName is None:
                    os.system('clear')
                    if len(player.get_hand()) > 0:
                        print ("You chose an invalid card number. Please try "
                               "again")
                    else:
                        print "All cards have been played already!"
                else:
                    os.system('clear')
                    print ("You have successfuly played the card %s!" % 
                          (cardName))
            elif (action.lower() == 'b'):
                #enter the buy menu
                print_buy_menu(player, centralLine)
                option = raw_input("Choose option: ")
                if option.lower() == 's':
                    #buy from central line's supplement
                    buy_supplement(player, centralLine)
                elif option.isdigit():
                    #buy from central line's active cards
                    if int(option) >= len(centralLine.get_active()):
                        os.system('clear')
                        print ("You entered an invalid number, please press"
                               "B to try another one")
                    else:
                        buy_active(player, centralLine, option)
                elif option.lower() == 'E':
                    #exit the buy menu
                    os.system('clear')
                    print "You didn't buy anything!"
                    break                    
                else:
                    #inform user about their mistake
                    os.system('clear')
                    print ("You entered an ivalid option. Please press B "
                           "if you still want to buy a card")

            elif action.lower() == 'a':
                #attack the opponent
                computer.set_health(computer.get_health()-player.get_attack())
                os.system('clear')
                print ("You attacked your opponent and dealt %s damage!" % 
                (player.get_attack()))
                player.set_attack(0)
            elif action.lower() == 'e':
                #end turn,transfer all cards to the discard pile
                player.to_discard()
                break
            elif action.lower() == 'q':
                #quit game
                os.system('clear')
                print "Bye!"
                exit()
            else:
                #inform the user about their mistake
                os.system('clear')
                print "Wrong option, choose one from the list"
   
        #initialise computer stats and deal cards
        os.system('clear')
        computer.draw_cards()
        computer.set_money(0)
        computer.set_attack(0)
        #transfer computer's cards from hand to the active area
        computer.play_cards()		

        print_computer_menu(computer)
        
        #attack the player
        player.set_health(player.get_health() - computer.get_attack()) 
        computer.set_attack(0)

        computer_purchase(computer, centralLine, aggressive)
        
        #end of turn, transfer all cards to the discard pile
        computer.to_discard()
        raw_input("\nComputer turn ended, press enter to continue")
        os.system('clear')
        
        #check for winner
        continueGame = winner(player, computer, centralLine)

        if continueGame == False:
            raw_input("Game ended, press enter to continue")
            os.system('clear')
            #ask user for another game
            continueGame = start_new_game()
            if continueGame == True:
                #initialise objects and deal central line's cards
                aggressive = opponent_type()
                player = Player('human player', 30, None, [], [], 5, [],0,0)
                computer = Player('computer', 30, None, [], [], 5, [],0,0)
                centralLine = CentralLine('central line', [], 5, None, None)
                centralLine.draw_active()
            else:
                print "Bye!"
        else:
            print "It's your turn to play again!"
		

    exit()









