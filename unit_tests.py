"""Unit test for the auxiliary functions and methods

This module contains unit tests that check the correctness of the auxiliary 
methods and functions

Methods:
    setUp -- Initialises objects that can be used by all the test cases

Unit tests:
    test_cards_moved_central_deck_to_central_active
    test_cards_moved_from_deck_to_hand
    test_all_cards_moved_from_hand_to_active
    test_one_card_moved_from_hand_to_active
    test_one_card_error_index_from_hand_to_active
    test_all_cards_moved_from_hand_and_active_to_discard
    test_winner_when_a_player_reaches_zero_health
    test_winner_when_a_player_reaches_negative_health
    test_winner_when_central_line_cards_are_over

Python unit testing guidelines can be found at: 
https://docs.python.org/3/library/unittest.html
"""

import unittest
from auxiliary_classes import Player, CentralLine
from auxiliary_functions import winner
unittest.TestLoader.sortTestMethodsUsing = None
 
class unit_tests(unittest.TestCase):
    
 
    def setUp(self):
        """Initialise object that can be used by all the test cases."""
        self.player = Player('player', 30, None, [], [], 5, [],0,0)
        self.computer = Player('computer', 30, None, [], [], 5, [],0,0)
        self.centralLine= CentralLine('central line', [], 5, None, None)

    def test_cards_moved_central_deck_to_central_active(self):
        """Check transfer of cards from central deck to central active.

        Must return 0 before dealing the cards. Must return 5 after dealing
        the cards.
        """
        self.assertEqual(len(self.centralLine.get_active()), 0)
        self.centralLine.draw_active()
        self.assertEqual(len(self.centralLine.get_active()), 5)


    def test_cards_moved_from_deck_to_hand(self):
        """Check transfer of cards from deck to hand.

        Must return 0 before dealing the cards. Must return 5 after dealing
        the cards.
        """

        self.assertEqual(len(self.player.get_hand()), 0)
        self.player.draw_cards()
        self.assertEqual(len(self.player.get_hand()), 5)

    def test_all_cards_moved_from_hand_to_active(self):
        """Check transfer of cards from hand to active.

        Must return 0 before the transfer of the cards. Must return 5 after 
        the transfer of the cards.
        """

        self.player.draw_cards()
        self.assertEqual(len(self.player.get_active()), 0)
        self.player.play_cards()      
        self.assertEqual(len(self.player.get_active()), 5)

    def test_one_card_moved_from_hand_to_active(self):
        """Check transfer of one card from hand to active.

        Must return 0 before the transfer of the cards. Must return 1 after 
        the transfer of the cards.
        """

        self.player.draw_cards()
        self.assertEqual(len(self.player.get_active()), 0)
        self.player.play_card(0)      
        self.assertEqual(len(self.player.get_active()), 1)

    def test_one_card_error_index_from_hand_to_active(self):
        """Check that the transfer of invalid card to active returns None"""
        self.player.draw_cards()
        self.assertEqual(len(self.player.get_active()), 0)     
        self.assertEqual(self.player.play_card(7), None)

    def test_all_cards_moved_from_hand_and_active_to_discard(self):
        """Check transfer of all cards from hand and active to discard.

        Must return 0 at discard before the transfer of the cards. Must
        return 1 after at active and 4 at hand before the transfer of the  
        cards. Must return 5 at discard and 0 at hand and active after the
        transfer of the cards.
        """

        self.player.draw_cards()
        self.player.play_card(0)      
        self.assertEqual(len(self.player.get_active()), 1)
        self.assertEqual(len(self.player.get_hand()), 4)
        self.player.to_discard()
        self.assertEqual(len(self.player.get_active()), 0)
        self.assertEqual(len(self.player.get_hand()), 0)
        self.assertEqual(len(self.player.get_discard()), 5)      
 
    def test_winner_when_a_player_reaches_zero_health(self):
        """Must return false if a player reached zero health"""
        computer = Player('computer', 0, None, [], [], 5, [],0,0)
        self.assertFalse(winner(self.player, computer, self.centralLine))

    def test_winner_when_a_player_reaches_negative_health(self):
        """Must return false if a player reached negative health"""
        player = Player('player', -4, None, [], [], 5, [],0,0)
        self.assertFalse(winner(player, self.computer, self.centralLine))

    def test_winner_when_central_line_cards_are_over(self):
        """Must return false if central line's cards are over"""
        centralLine= CentralLine('central line', [], 0, None, None)
        self.assertFalse(winner(self.player, self.computer, centralLine))
 
 
if __name__ == '__main__':
    unittest.main()
